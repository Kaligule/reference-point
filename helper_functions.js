find_all_matches_helper = function(string, reference) {
    var regexp_cardinality = '([0-9]+(?:[\.,][0-9])?)';
    var regex_whitespace = '(\\s*)';
    var regexp_unit = calculate_unit_regexp(reference);
    var completeRegExp = new RegExp(regexp_cardinality + regex_whitespace + regexp_unit, 'g');
    var matches = string.matchAll(completeRegExp);
    var matches_array = Array.from(matches);
    var match_references = matches_array.map(function(match) {
        return {match:match, reference:reference};
    });
    return match_references;
};


find_all_matches = function(string) {
    all_references_to_consider = [mass_reference, speed_reference, length_reference];
    result = [];
    for (let reference of all_references_to_consider) {
        matches_for_this_reference = find_all_matches_helper(string, reference);
        result = result.concat(matches_for_this_reference);
    };
    return result;
};

resolve_match_conflicts = function(list_of_matches) {
    var best_matches = [];
    for (let candidate_match of list_of_matches) {
        var candidate_is_defeated = false;
        for (let possibly_better_match of list_of_matches) {
            if (matches_do_conflict(candidate_match, possibly_better_match)) {
                if (is_better_match(possibly_better_match, candidate_match)) {
                    candidate_is_defeated = true;
                };
            };
        };
        if (!candidate_is_defeated) {
            best_matches.push(candidate_match);
        };
    };
    return best_matches;
};

matches_do_conflict = function(match_1, match_2) {
    var match_1_index = match_1.match["index"];
    var match_2_index = match_2.match["index"];
    var conflict = match_1_index == match_2_index;
    return conflict;
};

is_better_match = function(match_1, match_2) {
    var matched_string_1 = match_1.match[0];
    var matched_string_2 = match_2.match[0];
    return (matched_string_1.length > matched_string_2.length);
};

addReferencePointForMatch = function(string, match, reference) {
    // FIXME Should use named groups in the regexp instead of
    //       enumerating them but that doesn't seem to work on firefox
    let cardinality = parseFloat(match[1].replace(',', '.'));
    // let whitespace = match[2]; // not used at the moment
    let unit = match[3];
    let reference_string = calculate_reference_string(cardinality, unit, reference);
    string = string.replace(match[0], `${match[0]} (${reference_string})`);
    return string
};


// calculate a regex like '(mm|cm|m|km)[^a-z]' from the units in the reference
//units should match,
//words that start with the same letters (like _m_ango) shouldn't
calculate_unit_regexp = function(reference) {
    var units = Object.keys(reference["units"]);
    var word_end = '\\b';
    return '(' + units.join('|') + ')' + word_end;
};

// calculate a fitting reference string
// 8, "kg" -> "2x the mass of a cat"
calculate_reference_string = function(cardinality, unit, reference) {
    var prefered_reference_point = best_reference_point(cardinality, unit, reference);

    var unit_table = reference["units"];
    var factor = calculate_factor(cardinality, unit, unit_table, prefered_reference_point);
    factor = factor.toFixed(2); // round but it is not great yet
    var meassured_quantity = reference["measured quantity"];

    return `${factor}x the ${meassured_quantity} of a ${prefered_reference_point["what"]}`
};

// from all the reference points in the reference object, return the
// one best suited to compare with the mass represented by cardinality
// and unit.
best_reference_point = function(cardinality, unit, reference) {
    var potential_reference_points = reference["reference points"];
    var unit_table = reference["units"];

    var best_reference_point_yet = potential_reference_points[0];
    var best_factor_yet = calculate_factor(cardinality, unit, unit_table, best_reference_point_yet)
    for (let candidate_point of potential_reference_points) {
        let candidate_factor = calculate_factor(cardinality, unit, unit_table, candidate_point);
        if (is_better_factor(candidate_factor, best_factor_yet)) {
            best_reference_point_yet = candidate_point;
            best_factor_yet = candidate_factor;
        };
    };
    return best_reference_point_yet;
};

is_better_factor = function(factor, old_factor) {
    // A good factor should be bigger then 1 (so we can say "it is x times heavier the the reference point")
    // but as small as possible so we take a reference point that is similar to the measure.
    if (factor > 1 && old_factor < 1 ) {
        return true
    } else if (factor < 1 && old_factor > 1) {
        return false
    }
    return (factor < old_factor)
};

// calculate the quotient(factor) between the reference point and the mass represented by cardinality and unit.
// 8, "kg", {"unit": "g", "how much in unit": 100} -> 80
calculate_factor = function(cardinality, unit, unit_table, reference_point) {
    var original_quantity_in_base_units = cardinality * unit_table[unit];
    var reference_quantity_in_base_units = reference_point["how much in unit"] * unit_table[reference_point["unit"]];
    var factor = original_quantity_in_base_units / reference_quantity_in_base_units;
    return factor;
};
