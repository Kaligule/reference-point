data_files = data/*
references = references/mass.js references/speed.js references/length.js
# for submitting the plugin
sourcefiles = manifest.json add_reference_points.js helper_functions.js  icons/*.png

reference_points.zip: $(sourcefiles) $(references)
	zip --filesync reference_points.zip $(sourcefiles) $(references)

$(references): convert_to_json.py $(data_files)
	python3 convert_to_json.py mass speed length

clean:
	rm -f $(references) reference_points.zip
