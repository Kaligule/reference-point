String.prototype.addReferencePoints = function() {
    var result = this.slice(); // make a working copy of this string
    all_match_references = find_all_matches(result);
    all_match_references_to_act_on = resolve_match_conflicts(all_match_references);
    for (let match_reference of all_match_references_to_act_on) {
        match = match_reference.match;
        reference = match_reference.reference;
        result = addReferencePointForMatch(result, match, reference);
    };
    return result;
};


// create a TreeWalker of all text nodes
let allTextNodes = document.createTreeWalker(document.body, NodeFilter.SHOW_TEXT);

// iterate through all text nodes
while (allTextNodes.nextNode()) {
    // add reference points to the node
    let node = allTextNodes.currentNode;
    node.nodeValue = node.nodeValue.addReferencePoints()
}
