# Add reference points

A firefox extension to add reference points to measurements in
websites (inspired by [xkcd 526](https://xkcd.com/526/)).

It is listed on
[addons.mozilla.org](https://addons.mozilla.org/en-US/firefox/addon/reference-points/versions/),
so it is kind of official.

## Example

A website holding the text:

> I will buy 8 kg of apples.

Will be displayed as:

> I will buy 8 kg (2x the mass of a cat) of apples.

## Wording

__reference__: a json object as read from files in the references directory. It will contain two objects: "units" and "reference points"

__reference point__: a json object that describes a thing that could be used to compare it. Example:
```
{
    "what": "a cat",
    "unit": "kg",
    "weight in unit": "4"
}
```

__reference string__: a string comparing something to a reference point. Example: "8 kg (2x the mass of a cat)"

__match_reference__: an object `x` containing both a match (`x.match`) and the corresponding reference `x.reference`

## Releasing

1. Update the version number in the `manifest.json`
2. Make sure the `CHANGELOG.md` is updated
3. Create a zip file containing all the relevant files by running `make reference_points.zip`.
4. Log into
   [addons.mozilla.org](https://addons.mozilla.org/en-US/firefox/addon/reference-points)
   and upload the zip file. Fill out the form (using the CHANGELOG).
5. Wait until the review process from Mozilla has finished.

## Ideas

- Add more scales:
  - Money ("As much as a cheeseburger costs"), but this is difficult as money isn't worth the same everywhere
  - Area
  - Volume
  - pressure ("As much as you feel 1 meter under water")
  - force
  - power (horsepower)
  - energy: https://en.wikipedia.org/wiki/Orders_of_magnitude_(energy)
- Add noncummulative scales as well:
  - Timestamps ("2 years after WW2 ended")
  - Temperature ("Around body temperature", "Just under the melting point of water")
  - Noise ("louder then an ambulance sirene")
- Allow individual templates per scale for the reference-strings.
- Allow users to individualize the list of reference points.
- Have a mode to convert from imperial to metric units (and the other way round I guess).
- Link source for reference point in reference string.
