# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.5.2] - 2023-07-03
### Added
- add some imperial length units ()
- add the "width of an adult human finger" as reference point,
  because "length of a bee" came up very often and I wanted.

## [0.5.1] - 2020-10-24
### Added
- sources can now be given in the reference-point csv files.
- the reference points were redone - there are more of them now and
  they are more spaced out.

## [0.5] - 2020-10-13
### Added
- references are not longer written by hand but derived from csv-files
  in the data directory (which should to create them much more
  easily).
- Some fixpoints would match on the same string. For example in "I am
  driving 5 km/h." both "5 km" and "5 km/h" would be candidates for
  adding a reference point. Until now, both reference points would be
  added (which was confusing). These conflicts are now captured, only
  the longer string ("5 km/h") is used now.

## [0.4] - 2020-08-27
### Added
- numbers and units can now be separated by different types of
  whitespace (like tabs) where before only spaces would work.
- the number of whitespace characters separating number and unit
  doesn't matter anymore. Before only 0 or 1 was allowed.

### Fixed
- False positves where the beginning of a word looks like a unit: `1
  mighty sword` had looked like 1 meter and `ighty sword` before. This
  would result in the string to be replaced by `1 m (3.33x the length
  of a a cucumber)ighty sword`. This doesn't happen anymore'
- Formatting of numbers and whitespace is not changed anymore.
  Before `4,2g` would have been replaced by `4.2 g (...)`.


## [0.3] - 2020-08-23
### Added
- can now compare with lengths and masses. Other measurements can be
  added pretty easily.
- a 96x96 pixel icon (in addition to the preexisting 48x48 one)

## [0.2] - 2020-08-20
### Added

- can now compare with three different reference masses:
  a chocolate bar (100g), a cat (4kg) and a lady (60kg)

## [0.1] - 2020-08-19
### Added

- can compare masses in a text with a single reference mass
