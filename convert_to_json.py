import csv
import json
import os.path
import sys

def perhaps_float(input_string):
    """convert string to a float if possible"""
    try:
        return float(input_string)
    except ValueError:
        return input_string

def cleanup_dict(d):
    """remove clutter like leading or trailing spaces and convert float_strings to floats"""
    return dict(
        (
            (k.strip(), perhaps_float(v.strip()))
            for k, v
            in d.items()
        )
    )

def write_reference_file(measured_quantity):
    """Read data from the the csv files in the data directory,
    combine them into one json object and write it into a reference file."""
    with open(os.path.join('data', 'reference_points', f'{measured_quantity}.csv'), 'r') as csvfile:
        reference_points = csv.DictReader(csvfile)
        reference_points = [ cleanup_dict(row) for row in reference_points ]
    with open(os.path.join('data', 'unit_conversion', f'{measured_quantity}.csv'), 'r') as csvfile:
        unit_conversion = csv.reader(csvfile)
        unit_conversion = cleanup_dict(dict(unit_conversion))
    reference = {
        "measured quantity" : measured_quantity,
        "units" : unit_conversion,
        "reference points" : reference_points,
    }
    json_string = json.dumps(reference, indent=4)
    with open(os.path.join('references', f'{measured_quantity}.js'), 'w') as json_file:
        json_file.write(f'{measured_quantity}_reference = {json_string};')

def main(measured_quantities):
    for measured_quantity in measured_quantities:
        write_reference_file(measured_quantity)

if __name__ == "__main__":
    """
    Call this file with arguments like 'mass' or length'.
    It will be assumed that corresponding csv files exist in the data directory.
    """
    main(sys.argv[1:])
